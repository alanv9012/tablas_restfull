const express = require('express');

function list (req, res, next){
    res.send('Realizacion de API con tablas RESTfull');
}

function sum(req, res, next){
    res.send(`La suma ${req.params.n1} + ${req.params.n2} es = ${Number(req.params.n1) + Number(req.params.n2)}`);
}

function mult(req, res, next){
    res.send(`La Multiplicacion ${req.params.n1} * ${req.params.n2} es = ${Number(req.params.n1) * Number(req.params.n2)}`);
}

function div(req, res, next){
    res.send(`La division ${req.params.n1} / ${req.params.n2} es = ${Number(req.params.n1) / Number(req.params.n2)}`);
}

function pow(req, res, next){
    res.send(`La potencia ${req.params.n1} ^ ${req.params.n2} es = ${Number(req.params.n1) ** Number(req.params.n2)}`);
}

function min(req, res, next){
    res.send(`La resta ${req.params.n1} - ${req.params.n2} es = ${Number(req.params.n1) - Number(req.params.n2)}`);
}



module.exports = {
    list, sum, mult, div, pow, min
}