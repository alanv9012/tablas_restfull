const express = require('express');

function home(req, res, next){
    res.render('index', {title: 'Uso de REST'});
}

module.exports = {
    home
}