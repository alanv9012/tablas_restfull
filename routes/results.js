var express = require('express');
var router = express.Router();

const controller = require('../controllers/results');

/* GET users listing. */
router.get('/', controller.list);

router.get('/:n1/:n2', controller.sum);
router.post('/:n1/:n2', controller.mult);
router.put('/:n1/:n2', controller.div);
router.patch('/:n1/:n2', controller.pow);
router.delete('/:n1/:n2', controller.min);

module.exports = router;
